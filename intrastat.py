# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval


class SaleCostType(metaclass=PoolMeta):
    __name__ = 'sale.cost.type'

    declare_intrastat = fields.Boolean('Declare Intrastat',
        states={
            'invisible': (Eval('apply_method') != 'invoice_out')
        })

    @fields.depends('apply_method')
    def on_change_apply_method(self):
        if self.apply_method != 'invoice_out':
            self.declare_intrastat = False


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    def get_intrastat_goods_lines(self):
        pool = Pool()
        SaleCost = pool.get('sale.cost')

        lines = super().get_intrastat_goods_lines()
        if (not lines
                and isinstance(self.origin, SaleCost)
                and self.origin.type_.declare_intrastat):
            lines.extend([
                l for cost_line in self.origin.lines
                for l in cost_line.document_line.invoice_lines
                if l.invoice
                and l.invoice.type == self.invoice.type
                and l.invoice.party == self.invoice.party
                and l.invoice.state in ('posted', 'paid')
            ])
        return lines
