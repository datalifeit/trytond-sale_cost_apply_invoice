datalife_sale_cost_apply_invoice
================================

The sale_cost_apply_invoice module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_cost_apply_invoice/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_cost_apply_invoice)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
