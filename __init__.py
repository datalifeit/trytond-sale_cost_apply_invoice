# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale_cost
from . import invoice
from . import intrastat


def register():
    Pool.register(
        sale_cost.CostType,
        sale_cost.CostTemplate,
        sale_cost.SaleCost,
        module='sale_cost_apply_invoice', type_='model')
    Pool.register(
        invoice.InvoiceLineSaleInfo,
        module='sale_cost_apply_invoice', type_='model',
        depends=['account_invoice_line_sale_info'])
    Pool.register(
        sale_cost.SaleCostDiscount,
        module='sale_cost_apply_invoice', type_='model',
        depends=['account_invoice_discount'])
    Pool.register(
        intrastat.SaleCostType,
        intrastat.InvoiceLine,
        module='sale_cost_apply_invoice', type_='model',
        depends=['intrastat'])
    Pool.register(
        sale_cost.CostType349,
        invoice.InvoiceLine349,
        module='sale_cost_apply_invoice', type_='model',
        depends=['aeat_349'])
