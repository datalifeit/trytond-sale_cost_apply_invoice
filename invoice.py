# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from sql import Null
from sql.operators import Concat


class InvoiceLineSaleInfo(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    def get_shipment_out_numbers(self, name=None):
        SaleCost = Pool().get('sale.cost')

        if self.origin and isinstance(self.origin, SaleCost):
            shipments = set([shipment.rec_name
                for shipment in self.origin.document.shipments])
            return ','.join(sorted(shipments))
        return super().get_shipment_out_numbers(name)

    @classmethod
    def search_shipment_out_numbers(cls, name, clause):
        domain = super().search_shipment_out_numbers(name, clause)

        return ['OR',
            domain,
            [('origin.document.shipments.number',)
                + tuple(clause[1:3]) + ('sale.cost',) + tuple(clause[3:])]
        ]

    @classmethod
    def _get_order_shipment_out_numbers_query_items(cls, table):
        pool = Pool()
        StockMove = pool.get('stock.move')
        SaleLine = pool.get('sale.line')
        SaleCost = pool.get('sale.cost')
        ShipmentOut = pool.get('stock.shipment.out')
        sale_line = SaleLine.__table__()
        stock_move = StockMove.__table__()
        sale_cost = SaleCost.__table__()
        shipment_out = ShipmentOut.__table__()

        fromitem, conditionals = \
            super()._get_order_shipment_out_numbers_query_items(table)
        fromitem = fromitem.join(sale_cost, 'LEFT', condition=(
                Concat('sale.cost,', sale_cost.id) == table.origin)
            ).join(sale_line, 'LEFT', condition=(
                sale_line.sale == sale_cost.document)
            ).join(stock_move, 'LEFT', condition=(
                Concat('sale.line,', sale_line.id) == stock_move.origin)
            ).join(shipment_out, 'LEFT', condition=(
                Concat('stock.shipment.out,', shipment_out.id
                    ) == stock_move.shipment))
        conditionals.append((shipment_out.number != Null, shipment_out.number))
        return fromitem, conditionals

    @classmethod
    def _get_order_sale_date_query_items(cls, table):
        pool = Pool()
        Sale = pool.get('sale.sale')
        SaleCost = pool.get('sale.cost')
        sale = Sale.__table__()
        sale_cost = SaleCost.__table__()

        fromitem, conditionals = super()._get_order_sale_date_query_items(
            table)
        fromitem = fromitem.join(sale_cost, 'LEFT', condition=(
                Concat('sale.cost,', sale_cost.id) == table.origin)
            ).join(sale, 'LEFT', condition=(sale_cost.document == sale.id))
        conditionals.append((sale.sale_date != Null, sale.sale_date))
        return fromitem, conditionals

    def get_sale_data(self, name=None):
        if self.origin and self.origin.__name__ == 'sale.cost':
            return getattr(self.origin.document, name)
        return super().get_sale_data(name)

    @classmethod
    def search_sale_data(cls, name, clause):
        domain = super().search_sale_data(name, clause)
        return ['OR',
            domain,
            ('origin.document.%s' % clause[0],)
                + tuple(clause[1:3]) + ('sale.cost',) + tuple(clause[3:])]

    @classmethod
    def view_attributes(cls):
        attributes = super().view_attributes()
        for attribute in attributes:
            if attribute[0] == '/tree/field[@name="sale_date"]':
                attributes.remove(attribute)

        return attributes


class InvoiceLine349(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    def _get_aeat_349_ammendment_line(self):
        pool = Pool()
        SaleLine = pool.get('sale.line')
        SaleCost = pool.get('sale.cost')

        if (self.origin
                and isinstance(self.origin, SaleCost)
                and self.origin.type_.aeat_349_ammendment
                and self.origin.sale.invoices):
            for line in self.origin.sale.invoices[0].lines:
                if (line.type == 'line'
                        and line.origin
                        and isinstance(line.origin, SaleLine)
                        and line.origin.sale == self.origin.sale):
                    return line
            else:
                return

        return super()._get_aeat_349_ammendment_line()
