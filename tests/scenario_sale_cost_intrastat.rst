============================
Sale cost intrastat Scenario
============================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from proteus import Model
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.intrastat.tests.tools import configure_intrastat
    >>> today = datetime.date.today()

Install sale_cost::

    >>> config = activate_modules(['intrastat', 'sale_cost_apply_invoice'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']

Create tax::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create countries and subdivisions::

    >>> Subdivision = Model.get('country.subdivision')
    >>> SubdivisionType = Model.get('party.address.subdivision_type')
    >>> Country = Model.get('country.country')
    >>> country_es = Country(name="Spain", code="ES", eu_member=True)
    >>> country_es.save()
    >>> sub_type, = SubdivisionType.find([('country_code', '=', country_es.code)])
    >>> sub_type.types = list(sub_type.types) + ['state']
    >>> sub_type.save()
    >>> madrid = Subdivision(
    ...     name="Madrid", code="ES-MA", type='state', country=country_es)
    >>> madrid.save()

    >>> country_de = Country(name="Germany", code="DE", eu_member=True)
    >>> country_de.save()
    >>> berlin = Subdivision(
    ...     name="Berlin", code="DE-BE", type='state', country=country_de)
    >>> berlin.save()

Create parties and set addresses::

    >>> company_addr = company.party.addresses[0]
    >>> company_addr.country = country_es
    >>> company_addr.invoice = True
    >>> company_addr.delivery = True
    >>> company_addr.subdivision = madrid
    >>> company_addr.save()

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='customer')
    >>> customer.save()
    >>> address = customer.addresses[0]
    >>> address.invoice = True
    >>> address.delivery = True
    >>> address.country = country_de
    >>> address.subdivision = berlin
    >>> address.save()

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> wh.address = company_addr
    >>> wh.save()

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Configure Intrastat::

    >>> conf = configure_intrastat()
    >>> conf.declare_arrival = True
    >>> conf.declare_dispatch = True
    >>> conf.save()

Create Intrastat codes::

    >>> Code = Model.get('intrastat.code')
    >>> code_c1 = Code(code='04', description="DAIRY PRODUCE; BIRDS' EGGS; NATURAL HONEY; EDIBLE PRODUCTS OF ANIMAL ORIGIN,  NOT ELSEWHERE SPECIFIED OR INCLUDED")
    >>> code_c1.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> gram, = ProductUom.find([('name', '=', 'Gram')])
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'product 1'
    >>> template.categories.append(category)
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.intrastat_code = code_c1
    >>> template.save()
    >>> product1 = template.products[0]

    >>> template = ProductTemplate()
    >>> template.name = 'product 2'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.intrastat_code = code_c1
    >>> template.save()
    >>> product2 = template.products[0]

    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('50')
    >>> template.cost_price = Decimal('20')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.intrastat_code = code_c1
    >>> template.save()
    >>> service = template.products[0]
    >>> unit.digits = 2
    >>> unit.rounding = 0.01
    >>> unit.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create cost types::

    >>> CostType = Model.get('sale.cost.type')
    >>> type_comm = CostType(name='Commission')
    >>> type_comm.product = service
    >>> type_comm.formula = '0.2*quantity'
    >>> type_comm.apply_point = 'on_confirm'
    >>> type_comm.quantity_formula = '1'
    >>> type_comm.declare_intrastat = False
    >>> type_comm.apply_method = 'invoice_out'
    >>> type_comm.invoice_party = customer
    >>> type_comm.save()

Create cost templates::

    >>> CostTemplate = Model.get('sale.cost.template')
    >>> template = CostTemplate()
    >>> template.type_ = type_comm
    >>> template.party = customer
    >>> template.save()

Sale 2 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()

    >>> sale.sale_date = today
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product2
    >>> sale_line.quantity = 100.0
    >>> sale_line.unit = gram
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> sale.state
    'processing'

Check sale costs::

    >>> len(sale.costs)
    1

Check invoice and invoice lines::

    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> len(InvoiceLine.find([('origin', 'like', 'sale.line,%')]))
    2
    >>> len(InvoiceLine.find([('origin', 'like', 'sale.cost,%')]))
    1

Post invoices::

    >>> Invoice = Model.get('account.invoice')
    >>> sale_invoice, = sale.invoices
    >>> sale_invoice.click('post')
    >>> sale_invoice.state
    'posted'

    >>> costs_invoice = Invoice()
    >>> costs_invoice.party = customer
    >>> costs_invoice.lines.extend(
    ...     list(InvoiceLine.find([('origin', 'like', 'sale.cost,%')])))
    >>> costs_invoice.save()
    >>> costs_invoice.click('post')

Create Intrastat Declaration::

    >>> Declaration = Model.get('intrastat.declaration')
    >>> declaration = Declaration(from_date=today, to_date=today,
    ...     company=company, description='Declaration 1')
    >>> declaration.save()
    >>> declaration.state
    'draft'
    >>> declaration.click('create_lines')
    >>> len(declaration.lines)
    2

Add lines to declaration with the cost of other invoices services::

    >>> type_comm.declare_intrastat = True
    >>> type_comm.save()
    >>> declaration.click('delete_lines')
    >>> declaration.click('create_lines')
    >>> len(declaration.lines)
    4
    >>> abs(sum(l.amount for l in declaration.lines if l.amount < 0)) == sum(c.amount for c in sale.costs)
    True