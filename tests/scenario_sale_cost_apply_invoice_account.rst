========================================
Sale Cost Apply Invoice Account Scenario
========================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install sale_cost_apply_invoice::

    >>> config = activate_modules('sale_cost_apply_invoice')

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']


Create tax::

    >>> Tax = Model.get('account.tax')
    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()


Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> address = customer.addresses.new()
    >>> address.name = 'Address 2'
    >>> customer.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> kg, = ProductUom.find([('name', '=', 'Kilogram')])
    >>> gram, = ProductUom.find([('name', '=', 'Gram')])
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> template = ProductTemplate()
    >>> template.name = 'product 1'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product1 = template.products[0]

    >>> template = ProductTemplate()
    >>> template.name = 'product 2'
    >>> template.default_uom = kg
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product2 = template.products[0]

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create cost type invoice in::

    >>> CostType = Model.get('sale.cost.type')
    >>> type_invoice = CostType(name='Invoice')
    >>> type_invoice.account = expense
    >>> type_invoice.formula = '0.2*unknown_amount'
    >>> type_invoice.apply_method = 'invoice_in'
    >>> type_invoice.formula = '0.2*untaxed_amount'
    >>> type_invoice.invoice_party = supplier
    >>> type_invoice.save()


Create cost type invoice out::

    >>> type_invoice_out = CostType(name='Invoice out')
    >>> type_invoice_out.account = revenue
    >>> type_invoice_out.apply_method = 'invoice_out'
    >>> type_invoice_out.formula = '0.3*untaxed_amount'
    >>> type_invoice_out.invoice_party = customer
    >>> type_invoice_out.manual = True
    >>> type_invoice_out.save()


Create cost templates::

    >>> CostTemplate = Model.get('sale.cost.template')
    >>> template = CostTemplate()
    >>> template.type_ = type_invoice
    >>> template.invoice_party == supplier
    True
    >>> template.formula = '0.1*untaxed_amount'
    >>> template.party = customer
    >>> template.save()
    >>> template_out = CostTemplate()
    >>> template_out.type_ = type_invoice_out
    >>> template_out.invoice_party == customer
    True
    >>> template_out.party = customer
    >>> template_out.save()


Sale 2 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product1
    >>> sale_line.quantity = 2.0
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product2
    >>> sale_line.quantity = 300.0
    >>> sale_line.unit = gram
    >>> sale.click('quote')
    >>> sale.untaxed_amount, sale.tax_amount, sale.total_amount
    (Decimal('29.00'), Decimal('2.90'), Decimal('31.90'))

Check cost applying::

    >>> InvoiceLine = Model.get('account.invoice.line')
    >>> invoice_cost, = [c for c in sale.costs if c.type_ == type_invoice]
    >>> lines = InvoiceLine.find([('origin', 'like', 'sale.cost,%')])
    >>> len(lines)
    0
    >>> invoice_cost.invoice_party = supplier
    >>> invoice_cost.save()
    >>> sale.click('confirm')
    >>> line, = InvoiceLine.find([('origin', '=', 'sale.cost,%s' % invoice_cost.id)])
    >>> line.amount
    Decimal('2.90')
    >>> line.party == invoice_cost.invoice_party
    True
    >>> line.account == invoice_cost.type_.account
    True
    >>> invoice_out_cost, = [c for c in sale.costs
    ...     if c.type_ == type_invoice_out]
    >>> line, = InvoiceLine.find([('origin', '=', 'sale.cost,%s' % invoice_out_cost.id)])
    >>> line.amount
    Decimal('-8.70')