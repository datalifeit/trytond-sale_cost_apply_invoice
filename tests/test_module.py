# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class SaleCostApplyInvoiceTestCase(ModuleTestCase):
    """Test Sale Cost Apply Invoice module"""
    module = 'sale_cost_apply_invoice'
    extras = ['intrastat', 'aeat_349']


del ModuleTestCase
